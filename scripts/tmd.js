function tmd_dj_datatable(){
    jQuery(document).ready(function($) {
        // jQuery.fn.dataTable.ext.errMode = 'throw';

        var table = jQuery('#dj_table').DataTable({
            paging: true,
            deferRender: true,
            deferLoad: false,
            info: true,
            retrieve: true,
            stateSave: true,
            fixedHeader: {
                header: true,
                footer: true,
                headerOffset: 250,
            },
        });

    })

}
