<?php
/**
 * Created by PhpStorm.
 * User: dekay
 * Date: 25.06.17
 * Time: 15:40
 */

add_theme_support( 'post-thumbnails' );


add_image_size( 'dj-profile-small', 300, 300);
// to be used in tabular views of DJs

$factor = 1.5;
add_image_size( 'featured-image-header', 414*$factor, 157*$factor);
// is used as banner pciture for events

add_image_size( 'event_banner', 1200, 630, true );
// facebook og minimum dimensions