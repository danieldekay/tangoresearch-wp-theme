<?php
/**
 * Created by PhpStorm.
 * User: dekay
 * Date: 25.06.17
 * Time: 15:03
 */


function tmd_format_boolean ( $bool ){
	if ( $bool ) {
		return "<span class=\"label label-success\">yes</span>";
	} else {
		return "<span class=\"label label-danger\">no</span>";
	}
}
