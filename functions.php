<?php
/**
 * Functions
 *
 * @package      Bootstrap for Genesis
 * @since        1.0
 * @link         http://www.superfastbusiness.com
 * @author       SuperFastBusiness <www.superfastbusiness.com>
 * @copyright    Copyright (c) 2015, SuperFastBusiness
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */


add_action( 'genesis_setup', 'bfg_childtheme_setup', 15 );

function bfg_childtheme_setup() {
	// Start the engine
	include_once( get_template_directory() . '/lib/init.php' );
	
	// Child theme (do not remove)
	define( 'BFG_THEME_NAME', 'TMD4 based on Bootstrap for Genesis' );
	define( 'BFG_THEME_URL', 'http://www.superfastbusiness.com/' );
	define( 'BFG_THEME_LIB', CHILD_DIR . '/lib/' );
	define( 'BFG_THEME_LIB_URL', CHILD_URL . '/lib/' );
	define( 'BFG_THEME_IMAGES', CHILD_URL . '/images/' );
	define( 'BFG_THEME_JS', CHILD_URL . '/assets/js/' );
	define( 'BFG_THEME_CSS', CHILD_URL . '/assets/css/' );
	define( 'BFG_THEME_MODULES', CHILD_DIR . '/lib/modules/' );
	
	// Enqueue Google Fonts
	add_action( 'wp_enqueue_scripts', 'bfg_google_fonts' );
	function bfg_google_fonts() {
		wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Raleway:400,300,700,500|Roboto:400,400italic,700,700italic,300italic,300', array (), BFG_THEME_VERSION );
	}
	
	// Cleanup WP Head
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'start_post_rel_link' );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'adjacent_posts_rel_link' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );
	
	// Remove WP Version
	add_filter( 'the_generator', 'mb_remove_wp_version' );
	
	// Add HTML5 markup structure
	add_theme_support( 'html5', array ( 'search-form', 'comment-form', 'comment-list' ) );
	
	// Add viewport meta tag for mobile browsers
	add_theme_support( 'genesis-responsive-viewport' );
	
	// Add support for 3-column footer widgets
	add_theme_support( 'genesis-footer-widgets', 3 );
	
	// Structural Wraps
	add_theme_support( 'genesis-structural-wraps', array (
		'header',
		'nav',
		'subnav',
		'site-inner',
		'footer-widgets',
		'footer',
		'home-featured'
	) );
	
	// WooCommerce Support
	// add_theme_support( 'genesis-connect-woocommerce' );
	
	// Custom Image Size
	add_image_size( 'bootstrap-featured', 750, 422, true );
	
	// TGM Plugin Activation
	require_once( BFG_THEME_MODULES . 'tgm-plugin-activation.php' );
	
	// Bootstrap Shortcodes
	require_once( BFG_THEME_MODULES . 'shortcodes.php' );
	
	// Mr Image Resize
	require_once( BFG_THEME_MODULES . 'mr-image-resize.php' );
	
	// Customizer Helper
	require_once( BFG_THEME_MODULES . 'customizer-library/customizer-library.php' );
	
	// Include php files from lib folder
	// @link https://gist.github.com/theandystratton/5924570
	foreach ( glob( dirname( __FILE__ ) . '/lib/*.php' ) as $file ) {
		include $file;
	}
	
	// Move Sidebar Secondary After Content
	remove_action( 'genesis_after_content_sidebar_wrap', 'genesis_get_sidebar_alt' );
	add_action( 'genesis_after_content', 'genesis_get_sidebar_alt' );
	
	// Remove Gallery CSS
	add_filter( 'use_default_gallery_style', '__return_false' );
	
	// Add Shortcode Functionality to Text Widgets
	add_filter( 'widget_text', 'shortcode_unautop' );
	add_filter( 'widget_text', 'do_shortcode' );
	
	// Move Featured Image
	remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
	add_action( 'genesis_entry_header', 'genesis_do_post_image', 0 );
	
	
	function theme_prefix_rewrite_flush() {
		flush_rewrite_rules();
	}
	
	add_action( 'after_switch_theme', 'theme_prefix_rewrite_flush' );
	
	
	function wpse_allowedtags() {
		// Add custom tags to this string
		return '<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>';
	}
	
	function wpse_custom_wp_trim_excerpt( $wpse_excerpt ) {
		$raw_excerpt = $wpse_excerpt;
		if ( '' == $wpse_excerpt ) {
			
			$wpse_excerpt = genesis_get_custom_field( 'post_content' );
			$wpse_excerpt = strip_shortcodes( $wpse_excerpt );
			$wpse_excerpt = apply_filters( 'the_content', $wpse_excerpt );
			$wpse_excerpt = str_replace( ']]>', ']]&gt;', $wpse_excerpt );
			$wpse_excerpt = strip_tags( $wpse_excerpt, wpse_allowedtags() ); /*IF you need to allow just certain tags. Delete if all tags are allowed */
			
			//Set the excerpt word count and only break after sentence is complete.
			$excerpt_word_count = 50;
			$excerpt_length     = apply_filters( 'excerpt_length', $excerpt_word_count );
			$tokens             = array ();
			$excerptOutput      = '';
			$count              = 0;
			
			// Divide the string into tokens; HTML tags, or words, followed by any whitespace
			preg_match_all( '/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens );
			
			foreach ( $tokens[0] as $token ) {
				
				if ( $count >= $excerpt_length && preg_match( '/[\,\;\?\.\!]\s*$/uS', $token ) ) {
					// Limit reached, continue until , ; ? . or ! occur at the end
					$excerptOutput .= trim( $token );
					break;
				}
				
				// Add words to complete sentence
				$count ++;
				
				// Append what's left of the token
				$excerptOutput .= $token;
			}
			
			$wpse_excerpt = trim( force_balance_tags( $excerptOutput ) );
			
			$excerpt_end  = '<span class="label label-warning">login for more</span><br/> <a href="' . esc_url( '/wp-login.php' ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf( __( 'Please log in to read all about: %s &nbsp;&raquo;', 'wpse' ), get_the_title() ) . '</a>';
			$excerpt_more = apply_filters( 'excerpt_more', ' ' . $excerpt_end );
			
			//$pos = strrpos($wpse_excerpt, '</');
			//if ($pos !== false)
			// Inside last HTML tag
			//$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
			//else
			// After the content
			$wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */
			
			return $wpse_excerpt;
			
		}
		
		return apply_filters( 'wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt );
	}
}


/**
 * Get taxonomies terms links.
 *
 * @see get_object_taxonomies()
 */
function wpdocs_custom_taxonomies_terms_links() {
	// Get post by post ID.
	$post = get_post( $post->ID );
	
	// Get post type by post.
	$post_type = $post->post_type;
	
	// Get post type taxonomies.
	$taxonomies = get_object_taxonomies( $post_type, 'objects' );
	
	$out = array ();
	
	foreach ( $taxonomies as $taxonomy_slug => $taxonomy ) {
		
		// Get the terms related to post.
		$terms = get_the_terms( $post->ID, $taxonomy_slug );
		
		if ( ! empty( $terms ) ) {
			$out[] = "<h2>" . $taxonomy->label . "</h2>\n<ul>";
			foreach ( $terms as $term ) {
				$out[] = sprintf( '<li><a href="%1$s">%2$s</a></li>',
					esc_url( get_term_link( $term->slug, $taxonomy_slug ) ),
					esc_html( $term->name )
				);
			}
			$out[] = "\n</ul>\n";
		}
	}
	
	return implode( '', $out );
}